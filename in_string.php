<?php

function in_string($needle, $haystack, $sensitive = true) {

    if ($sensitive) {

        if (strpos($needle, $haystack) !== false) return true;
        else return false;

    } else {

        if (stripos($needle, $haystack) !== false) return true;
        else return false;

    }

}