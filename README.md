# in_string

strpos is the fastest way to search for a needle in a haystack, or `string in string`.

### Installing

Place the in_string() function in your code, or include the in_string.php file.


## Running a test

```

$inMyLongString = 'Does this contain findMe?!?';

if (in_string('findMe', $inMyLongString)) {
    echo 'Sí Señor!';
}

```

Will output

```
Sí Señor!
```

## Authors
* **Maarten Soetens** 
* [Company](https://woensdag.nl)
* [Gitlab](https://gitlab.com/Soetens)
* [LinkedIn](https://www.linkedin.com/in/soetens)
* [Freelance PHP Developer](https://maarten.online)

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE - see the [LICENSE.md](LICENSE.md) file for details